(*
Export[
	"edge.jpg",
	EdgeDetect[
		Import[
			"IMG_4365.CR2"
		],
		20
	]
]
*)


f[x_] := Export [
	StringJoin["art_", IntegerString[x], ".jpg"],
	ImageAdd[

		Import[
			"IMG_4364.CR2"
		],
		
		EdgeDetect[
			Import[
				"IMG_4365.CR2"
			],
			x
		]
	]
];

f[20]
